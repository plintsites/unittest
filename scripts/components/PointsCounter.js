import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

/*
    PointsCounter component: displayed when Row completed (and other player not) to count the points
    <PointsCounter />
 */

const PointsCounter = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        status: React.PropTypes.string.isRequired,
        amount: React.PropTypes.number.isRequired,
        completedByOpponent: React.PropTypes.bool.isRequired,
        number: React.PropTypes.number.isRequired,
        addPoints: React.PropTypes.func.isRequired
    },

    render: function() {
        var pointsClass = 'points-container row-item ' + this.props.status;
        var points = <span className="points-amount">{this.props.amount}</span>
        var button = '';
        if (!this.props.completedByOpponent) {
            button = <button type="button" onClick={this.props.addPoints.bind(null, this.props.number)}><span className="glyphicon glyphicon-plus"></span></button>
        }

        return (
            <span className={pointsClass}>
                {button}
                {points}
            </span>
        )
    }
});

export default PointsCounter;
