import './setup';

import React from 'react';
// import TestUtils from 'react-addons-test-utils';
import {shallow, mount} from 'enzyme';
import Immutable from 'immutable';

import h from '../lib/helpers';
import th from './testhelpers';

import Finished from '../components/Finished';

import test from 'tape';

function shallowRenderFinished(player1, player2, resetGame) {
    return shallow(<Finished player1={player1} player2={player2} resetGame={resetGame} />);
}

export function testFinished() {
    test('Finished component', function(t) {

        // create a player instance
        const player1Tmp = h.createPlayer('lim');
        // overwrite the score => score = 55
        player1Tmp.score = th.getTestScore();
        // Make an immutable structure
        const player1 = Immutable.fromJS(player1Tmp);
        // create player2
        const player2 = Immutable.fromJS(h.createPlayer());
        // create callback function
        const resetGame = function(){};
        // shallow render it
        const result = shallowRenderFinished(player1, player2, resetGame);

        // the actual tests
        t.test('-- The outermost div should have a class named overlay-container', (t) => {
            t.plan(1);
            t.equal(result.hasClass('overlay-container'), true);
        });

        t.test('-- The first player is called lim', (t) => {
            t.plan(1);
            t.equal(result.find('.popup-standings .row').first().find('.col-md-4').first().text(), 'lim');
        });

        t.test('-- The first player has exactly 55 points', (t) => {
            t.plan(1);
            t.equal(parseInt(result.find('.popup-standings .row').first().find('.score').text()), 37);
        });

    });
};
