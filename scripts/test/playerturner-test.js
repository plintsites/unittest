import './setup';

import React from 'react';

import {shallow, mount} from 'enzyme';

import PlayerTurner from '../components/PlayerTurner';

import test from 'tape';

function shallowRender(changeTurn) {
    return shallow(<PlayerTurner changeTurn={changeTurn} />);
}


export function testPlayerTurner() {
    test('PlayerTurner component', (t) => {

        const changeTurnEmpty = function() {};
        // shallow render the component
        const result = shallowRender(changeTurnEmpty);

        t.test('-- Contains an <img> with src one-finger-tap', (t) => {
            t.plan(1);
            t.ok(result.find('img').prop('src').indexOf('one-finger-tap') > -1);
        });

        t.test('-- Clicking the PlayerTurner calls the given prop', (t) => {
            t.plan(1);

            const changeTurn = function(){
                // Set a stupid assertion, this only returns a successfull test
                // if the function is called
                t.ok(15 > 3);
            };

            // mount component
            const resultdom = mount(<PlayerTurner changeTurn={changeTurn} />);

            resultdom.find('.turner-container').simulate('click');
        });
    });
};


