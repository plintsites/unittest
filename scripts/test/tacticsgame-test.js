import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import TacticsGame from '../components/TacticsGame';
import h from '../lib/helpers';
import Immutable from 'immutable';
import test from 'tape';

export function testTacticsGame() {
    test('TacticsGame component', (t) => {

        const turn = true;
        const player1 = Immutable.fromJS(h.createPlayer('Lim'));
        const player2 = Immutable.fromJS(h.createPlayer('De Pan'));
        const changeTurn = () => {};
        const hitNumber = () => {};
        const addPoints = () => {};

        const result = shallow(<TacticsGame
            player1={player1}
            player2={player2}
            turn={turn}
            changeTurn={changeTurn}
            hitNumber={hitNumber}
            addPoints={addPoints}
        />);

        // 1] There is a div with class gamefields
        t.test('-- There is a div with class gamefields', (t) => {
            t.plan(1);
            t.ok(result.find('.tactics-container').children().first().hasClass('gamefields'));
        });

    });
}

