import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import Tactics from '../components/Tactics';
import h from '../lib/helpers';
import th from './testhelpers';
import Immutable from 'immutable';
import test from 'tape';

export function testTactics() {
    test('Tactics component', (t) => {

        const result = shallow(<Tactics />);

        t.test('-- Testing initial rendering', (t) => {
            // App is rendered with a <Header /> and a <TacticsGame /> component
            t.plan(1);
            t.ok(result.hasClass('tactics'));
        });

        t.test('-- Test changing turn', (t) => {
            // Make the other PlayerField active
            const app = mount(<Tactics />);

            t.test('++++ Left player is active initially', (t) => {
                t.plan(1);
                t.ok(app.find('.gamefield').first().hasClass('turn'));
            });

            t.test('++++ Changing player makes the other gamefield active', (t) => {
                t.plan(1);
                const button = app.find('.turner-container');
                button.simulate('click');
                // t.ok(!app.find('.gamefield').first().hasClass('turn'));
                t.ok(app.find('.gamefield').last().hasClass('turn'));
            });

        });

        t.test('-- Test hitting a number', (t) => {
            const app = mount(<Tactics />);
            // hitting a number => cross it out
            t.test('++++ Hit a number, cross it out', (t) => {
                t.plan(3);
                const number = app.find('.gamefield.turn .tactics-row').last().find('.row-item').first();
                t.ok(number.hasClass('hit-0'));
                // hit a number
                number.simulate('click');
                t.ok(number.hasClass('hit-1'));
                t.ok(app.find('.hit-1').length === 3);
            });

            // hitting the row complete => row turns green
            t.test('++++ Complete a row, make it green', (t) => {
                t.plan(1);
                const row = app.find('.gamefield.turn .tactics-row').last();
                const number = row.find('.row-item').first();
                // hit it three times
                number.simulate('click');
                number.simulate('click');
                number.simulate('click');

                t.ok(row.hasClass('row-completed'));
            });
        });

        t.test('-- Test scoring points', (t) => {
            const app = mount(<Tactics />);
            // Scoring points increases the total points for a player
            t.plan(4);
            const activeField = app.find('.gamefield.turn');
            const row = activeField.find('.tactics-row').last();
            const number = row.find('.row-item').first();
            const pointsCounter = row.find('.points-container');
            const button = pointsCounter.find('button');

            t.ok(pointsCounter.hasClass('hidden'));

            // hit it three times
            number.simulate('click');
            number.simulate('click');
            number.simulate('click');

            t.ok(!pointsCounter.hasClass('hidden'));

            button.simulate('click');
            button.simulate('click'); // => 20 points for player 1

            t.equal(pointsCounter.find('.points-amount').text(), '20');

            t.equal(activeField.find('h2 .player-points').text(), '20');
        });

        t.test('-- Test resetting game', (t) => {
            const app = mount(<Tactics />);

            t.plan(7);

            var nearlyFinishedData = {
                startTurn: true,
                gameTurn: true,
                gameFinished: false,
                player1: h.createPlayer('Player 1'),
                player2: h.createPlayer('Player 2'),
            };

            // ---
            // change some data to simulate end of game
            // ---
            nearlyFinishedData.player1.score = th.getTestScore();
            nearlyFinishedData.player2.score = th.getFinishScore();
            nearlyFinishedData.player2.roundsWon = 2;

            app.setState({
                data: Immutable.fromJS(nearlyFinishedData)
            });

            // Finish the game by hitting the last number
            const activeField = app.find('.gamefield.turn');
            const row = activeField.find('.tactics-row').first();
            const number = row.find('.row-item').first();

            // Check number of points of player 1
            t.ok(activeField.find('h2 .player-points').text() === '37')

            number.simulate('click'); // => This will finish the game and show the Finish screen

            t.ok(app.find('.popup-standings .score').first().text() === '37');
            t.ok(app.find('.popup-standings .rounds-won').first().text() === '1');
            t.ok(app.find('.popup-standings .rounds-won').last().text() === '2');

            // Simulate clicking the button to restart
            app.find('.popup-buttons button.btn-warning').simulate('click');

            // Points set to zero
            t.ok(activeField.find('h2 .player-points').text() === '0')
            // All <Row /> components are not completed, all numbers cleaned up
            t.ok(app.find('.tactics-row.row-completed').length === 0)
            // No <PointsCounter /> components available
            t.ok(app.find('.points-container').first().hasClass('hidden'));
        });

    });

}

