import { testFinished } from './finished-test';
import { testPlayerTurner } from './playerturner-test';
import { testHeader } from './header-test';
import { testPlayerField } from './playerfield-test';
import { testPointsCounter } from './pointscounter-test';
import { testStandings } from './standings-test';
import { testRow } from './row-test';
import { testTactics } from './tactics-test';
import { testTacticsGame } from './tacticsgame-test';


testPlayerTurner();
testFinished();
testHeader();
testPlayerField();
testPointsCounter();
testStandings();
testRow();
testTactics();
testTacticsGame();
