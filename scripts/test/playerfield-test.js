import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import PlayerField from '../components/PlayerField';
import h from '../lib/helpers';
import th from './testhelpers';
import Immutable from 'immutable';
import test from 'tape';

export function testPlayerField() {
    test('PlayerField component', (t) => {
        // SIMPLE TESTS
        // 1] A PlayerField contains 11 Rows
        // 2] An active PlayerField has a class 'turn'
        // 3] A PlayerField has a name
        // 4] A PlayerField has a score

        const turn = true;
        const player = Immutable.fromJS(h.createPlayer('Lim'));
        const hitNumber = () => {};
        const addPoints = () => {};

        const result = shallow(<PlayerField player={player} turn={turn} hitNumber={hitNumber} addPoints={addPoints}/>);
        const resultdom = mount(<PlayerField player={player} turn={turn} hitNumber={hitNumber} addPoints={addPoints}/>);

        t.test('-- A PlayerField contains 10 Rows', (t) => {
            t.plan(1);
            t.equal(resultdom.find('.tactics-row').length, 11);
        });

        t.test('-- An active PlayerField has a class turn', (t) => {
            t.plan(1);
            t.ok(result.hasClass('turn'));
        });

        t.test('-- An initial PlayerField has a name and score 0', (t) => {
            t.plan(1);
            t.equal(result.find('h2').text(), 'Lim (0)');
        });

        t.test('-- Further in the game there are points', (t) => {
            t.plan(1);

            const playerTmp = h.createPlayer('Lim');
            playerTmp.score = th.getTestScore();
            // Make an immutable structure
            const player = Immutable.fromJS(playerTmp);

            const result = shallow(<PlayerField player={player} turn={turn} hitNumber={hitNumber} addPoints={addPoints}/>);

            t.equal(parseInt(result.find('.player-points').text()), 37);
        });

    });
}

