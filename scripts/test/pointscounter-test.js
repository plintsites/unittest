import './setup';

import React from 'react';
import { shallow, mount } from 'enzyme';

import PointsCounter from '../components/PointsCounter';

import test from 'tape';

export function testPointsCounter() {
    test('PointsCounter component', (t) => {

        // Create the component by mounting it, than setup tests
        const amount = 54;
        const number = 18;
        const status = '';
        const statusHidden = 'hidden';
        const addPoints = function(){};
        const opponent1 = false;
        const opponent2 = true;

        const result1 = shallow(<PointsCounter number={number} amount={amount} status={status} completedByOpponent={opponent1} addPoints={addPoints} />);
        const result2 = shallow(<PointsCounter number={number} amount={amount} status={status} completedByOpponent={opponent2} addPoints={addPoints} />);
        const resultHidden = shallow(<PointsCounter number={number} amount={amount} status={statusHidden} completedByOpponent={opponent2} addPoints={addPoints} />);



        t.test('-- This player has 54 points', (t) => {
            t.plan(1);
            t.equal(parseInt(result1.find('.points-amount').text()), 54);
        });

        t.test('-- The players opponent did not yet complete this number', (t) => {
            t.plan(1);
            // t.ok(result1.find({type: 'button'}));
            t.ok(result1.find('button > span').hasClass('glyphicon'));
        });

        t.test('-- The component is hidden', (t) => {
            t.plan(1);
            t.ok(resultHidden.hasClass('hidden'));
        });

        t.test('-- Clicking the addPoints button calls the given prop', (t) => {
            t.plan(1);

            const addPoints = function(){
                // Set a stupid assertion, this only returns a successfull test
                // if the function is called
                t.ok(15 > 3);
            };

            // mount component
            const resultdom = mount(<PointsCounter number={number} amount={amount} status={status} completedByOpponent={opponent1} addPoints={addPoints} />);

            resultdom.find('button').simulate('click');
        });
    });
}

